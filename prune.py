import torch
import torch.nn as nn
from torch.nn.utils import prune

import copy

def getOrigWeights(model):
    """
    Gets the weights of the matrix and prevents them from being updated by training
    """
    return copy.deepcopy(model.state_dict())

def rewindOrigWeights(model, orig):
    """
    Rewinds the weights of the model back to the given weights without changing the mask
    """

    for k, v in model.state_dict().items():
        if 'orig' not in k: # Prevent masks from reverting
            orig[k] = v

    model.load_state_dict(orig)

    return model


def getPruneAmount(model):
    """
    Returns the percentage of the model that has been pruned
    """
    total = 0
    s = 0
    for k, v in model.state_dict().items():
        if 'orig' not in k: # Ignore masks
            total += torch.numel(v)
            s += torch.sum(v)
    return 1 - s / total


def initOrig(model):
    """
    Initialize the model parameters to be in "pruned" form
    """
    for module in model.modules():
        if isinstance(module, nn.Conv2d):
            prune.identity(module, 'weight')
            prune.identity(module, 'bias')
        if isinstance(module, nn.Linear):
            prune.identity(module, 'weight')
            prune.identity(module, 'bias')
    return model

def applyPrune(model, layer_type, proportion, method, createCopy=False, debug=False):
    """
    Applies the given prune method to the model.
    Created for easy debugging
    """
    if debug:
        total = 0
        prev = 0
        for k, v in model.state_dict().items():
            if 'orig' not in k:
                total += torch.numel(v)
                prev += torch.sum(v)

    if createCopy:
        orig = copy.deepcopy(model)
        pruned = method(model, layer_type, proportion)
    else:
        pruned = method(model, layer_type, proportion)

    
    if debug:
        cur = 0
        for k, v in model.state_dict().items():
            if 'orig' not in k:
                cur += torch.sum(v)
        print("Num elements before pruning: {:.0f}".format(prev))
        print("Num elements after pruning: {:.0f}".format(cur))
        print("Percentage cut: {:.4f}".format((1 - cur / prev).item()))
        print("Prune total: {:.4f}".format(  (1 - cur / total).item()  ))

    if createCopy:
        return pruned, orig
    return pruned

# https://spell.ml/blog/model-pruning-in-pytorch-X9pXQRAAACIAcH9h
def prune_model_l1_unstructured(model, layer_type, proportion):
    '''
        Prune the model by layer
    '''
    for module in model.modules():
        if isinstance(module, layer_type):
            prune.l1_unstructured(module, 'weight', proportion)
    return model

def prune_model_global_unstructured(model, layer_type, proportion):
    '''
        Prune the model by grouping layers to prune simultaneously
    '''
    module_tups = []
    for module in model.modules():
        if isinstance(module, layer_type):
            module_tups.append((module, 'weight'))

    if module_tups != []:
        prune.global_unstructured(
            parameters=module_tups, pruning_method=prune.L1Unstructured,
            amount=proportion
        )
    return model

def prune_model_l1_unstructured_reduced_last(model, layer_type, proportion, reduction=2):
    '''
        Prune the model by layer, reduce rate on output layer
    '''
    cur = None
    for mod in model.modules():
        if cur == None and isinstance(mod, layer_type):
            cur = mod
        elif cur != None and isinstance(mod, layer_type):
            prune.l1_unstructured(cur, 'weight', proportion)
            cur = mod
    prune.l1_unstructured(cur, 'weight', proportion / reduction)
    return model

def prune_model_universal_unstructured(model, layer_type, proportion):
    '''
        Prune the model globally
    '''
    module_tups = []
    for module in model.modules():
        if isinstance(module, nn.Linear) or isinstance(module, nn.Conv2d):
            module_tups.append((module, 'weight'))

    if module_tups != []:
        prune.global_unstructured(
            parameters=module_tups, pruning_method=prune.L1Unstructured,
            amount=proportion
        )
    return model
