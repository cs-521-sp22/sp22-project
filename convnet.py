import torch
import torch.nn as nn
import torch.nn.functional as F


tanh = torch.tanh

class Conv2(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=64, kernel_size=3,
                                padding=1)
        self.conv2 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3,
                                padding=1)
        
        self.fc1 = nn.Linear(16384, 256)
        self.fc2 = nn.Linear(256, 256)
        self.fc3 = nn.Linear(256, 10)
        
        self.pool2d = nn.MaxPool2d(kernel_size=2, stride=2)
        self.relu = nn.ReLU()

    def forward(self, x):

        x =           self.relu(self.conv1(x))
        x = self.pool2d(self.relu(self.conv2(x)))

        x = torch.flatten(x, 1)

        x = self.relu(self.fc1(x))
        x = self.relu(self.fc2(x))
        x =           self.fc3(x)

        return x, F.softmax(x, dim=1)
        
        
class Conv4(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=64, kernel_size=3,
                                padding=1)
        self.conv2 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3,
                                padding=1)
        self.conv3 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3,
                                padding=1)
        self.conv4 = nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3,
                                padding=1)
        
        self.fc1 = nn.Linear(8192, 256)
        self.fc2 = nn.Linear(256, 256)
        self.fc3 = nn.Linear(256, 10)
        
        self.pool2d = nn.MaxPool2d(kernel_size=2, stride=2)
        self.relu = nn.ReLU()

    def forward(self, x):

        x = self.relu(self.conv1(x))
        x = self.relu(self.conv2(x))
        x = self.pool2d(x)

        x = self.relu(self.conv3(x))
        x = self.relu(self.conv4(x))
        x = self.pool2d(x)

        x = torch.flatten(x, 1)

        x = self.relu(self.fc1(x))
        x = self.relu(self.fc2(x))
        x =           self.fc3(x)

        return x, F.softmax(x, dim=1)
        
        
class Conv6(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=64, kernel_size=3,
                                padding=1)
        self.conv2 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3,
                                padding=1)
        self.conv3 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3,
                                padding=1)
        self.conv4 = nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, 
                                padding=1)
        self.conv5 = nn.Conv2d(in_channels=128, out_channels=256, kernel_size=3,
                                padding=1)
        self.conv6 = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3,
                                padding=0)
        
        self.fc1 = nn.Linear(2304, 256)
        self.fc2 = nn.Linear(256, 256)
        self.fc3 = nn.Linear(256, 10)
        
        self.pool2d = nn.MaxPool2d(kernel_size=2, stride=2)
        self.relu = nn.ReLU()

    def forward(self, x):

        x = self.relu(self.conv1(x))
        x = self.relu(self.conv2(x))
        x = self.pool2d(x)

        x = self.relu(self.conv3(x))
        x = self.relu(self.conv4(x))
        x = self.pool2d(x)

        x = self.relu(self.conv5(x))
        x = self.relu(self.conv6(x))
        x = self.pool2d(x)

        x = torch.flatten(x, 1)

        x = self.relu(self.fc1(x))
        x = self.relu(self.fc2(x))
        x =           self.fc3(x)

        return x, F.softmax(x, dim=1)
        