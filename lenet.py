import torch
import torch.nn as nn
import torch.nn.functional as F


tanh = torch.tanh

class LeNet300(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(784, 300)
        self.fc2 = nn.Linear(300, 100)
        self.fc3 = nn.Linear(100, 10)

    def forward(self, x):
        x = x.view(-1, 784)
        
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x =        self.fc3(x)
        
        return x, F.softmax(x, dim=1)


class LeNet5(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(in_channels=1, out_channels=6, kernel_size=5,
                                padding=2)
        self.conv2 = nn.Conv2d(in_channels=6, out_channels=16, kernel_size=5,
                                padding=0)
        self.conv3 = nn.Conv2d(in_channels=16, out_channels=120, kernel_size=5,
                                padding=0)
        
        self.fc1 = nn.Linear(120, 84)
        self.fc2 = nn.Linear(84, 10)
        
        self.pool = nn.AvgPool2d(kernel_size=2, stride=2)
        self.sigmoid = nn.Sigmoid()

    def forward(self, x):

        x = self.pool(tanh(self.conv1(x)))
        x = self.pool(tanh(self.conv2(x)))
        x =           tanh(self.conv3(x))

        x = torch.flatten(x, 1)

        x = tanh(self.fc1(x))
        x = tanh(self.fc2(x))

        return x, F.softmax(x, dim=1)
