import os
import prune
import pickle

import torch
import torchvision
import torchvision.transforms as transforms

import copy

def average(lst): 
    if len(lst) == 0:
        return 0
    return sum(lst)/len(lst)

def saveModel(model, make, type, version):
    """
    Saves the model to disk
    """
    path = f"./results/{make}/{type}" 
    if not os.path.exists(path):
        # Create a new directory because it does not exist 
        os.makedirs(path)
    torch.save(model, path + f"/{version:.3f}.pth")

def loadModel(make, type, version):
    """
    Loads and returns the model specified by the make and model
    """
    path = f"./results/{make}/{type}/{version:.3f}.pth"
    if not os.path.exists(path):
        raise Exception("Desired model does not exists")
    return torch.load(path)


def saveDict(dict, make, type, mode, version=None):
    """
    Saves the dictionary to disk
    """
    path = f"./results/{make}/{type}" 
    if not os.path.exists(path):
        # Create a new directory because it does not exist 
        os.makedirs(path)
        
    if version == None:
        with open(path + f"/{mode}.pickle", 'wb') as handle:
            pickle.dump(dict, handle)
    else:
        with open(path + f"/{mode}_{version}.pickle", 'wb') as handle:
            pickle.dump(dict, handle)

def saveIterDict(dict, make, type, version=None):
    """
    Saves the iteration dictionary to disk
    """
    saveDict(dict, make, type, 'iter', version)

def saveAccDict(dict, make, type, version=None):
    """
    Saves the accuracy dictionary to disk
    """
    saveDict(dict, make, type, 'acc', version)


def loadDict(make, type, mode, version=None):
    """
    Loads the dictionary from disk
    """
    if version == None:
        path = f"./results/{make}/{type}/{mode}.pickle" 
    else:
        path = f"./results/{make}/{type}/{mode}_{version}.pickle" 

    if not os.path.exists(path):
        # Create a new directory because it does not exist 
        return {}
    with open(path, 'rb') as handle:
        return pickle.load(handle)

def loadIterDict(make, type, version=None):
    """
    Loads the iteration dictionary from disk
    """
    return loadDict(make, type, 'iter', version)

def loadAccDict(make, type, version=None):
    """
    Loads the accuracy dictionary from disk
    """
    return loadDict(make, type, 'acc', version)


def loadMnist(batch_size=64, class_labels=False):
    """
    Returns the train and test dataloaders for MNIST
    """
    transform = torchvision.transforms.Compose([
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize(
            (0.1307,), (0.3081,))])

    train_set = torchvision.datasets.MNIST('./mnist', train=True, transform=transform, download=True)
    train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size, shuffle=True)
    
    test_set = torchvision.datasets.MNIST('./mnist', train=False, transform=transform)
    test_loader = torch.utils.data.DataLoader(test_set, batch_size=batch_size, shuffle=True)
    
    classes = ('1', '2', '3', '4', '5', '6', '7', '8', '9', '0')

    if class_labels:
        return train_loader, test_loader, classes
    return train_loader, test_loader
    
def loadCifar(batch_size=64, class_labels=False):
    """
    Returns the train and test dataloaders for Cifar-10
    """
    transform = transforms.Compose(
        [transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    train_set = torchvision.datasets.CIFAR10('./cifar-10', train=True, transform=transform, download=True)
    train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size, shuffle=True)
    
    test_set = torchvision.datasets.CIFAR10('./cifar-10', train=False, transform=transform)
    test_loader = torch.utils.data.DataLoader(test_set, batch_size=batch_size, shuffle=True)
    
    classes = ('plane', 'car', 'bird', 'cat',
            'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

    if class_labels:
        return train_loader, test_loader, classes
    return train_loader, test_loader


def get_accuracy(model, data_loader, device, count=-1):
    '''
    Function for computing the accuracy of the predictions over the entire data_loader
    '''
    
    correct_pred = 0 
    n = 0
    c = 0
    
    with torch.no_grad():
        model.eval()
        for X, y_true in data_loader:
            c += 1

            X = X.to(device)
            y_true = y_true.to(device)

            _, y_prob = model(X)
            _, predicted_labels = torch.max(y_prob, 1)

            n += y_true.size(0)
            correct_pred += (predicted_labels == y_true).sum()
            if c == count: # Shorten validation for quicker calcs
                break

    return correct_pred.float() / n

def validate(valid_loader, model, criterion, device, count=-1):
    '''
    Function for the validation step of the training loop
    '''
   
    model.eval()
    running_loss = 0
    n = 0
    c = 0
    
    for X, y_true in valid_loader:
        c += 1
    
        X = X.to(device)
        y_true = y_true.to(device)
        
        n += y_true.size(0)

        # Forward pass and record loss
        y_hat, _ = model(X) 
        loss = criterion(y_hat, y_true) 
        running_loss += loss.item() * X.size(0)
        if c == count: # Shorten validation for quicker calcs
            break

    epoch_loss = running_loss / n
        
    return model, epoch_loss

def training_loop(model, criterion, optimizer, train_loader, valid_loader, epochs, device, print_every=1):
    '''
    Function defining the entire training loop
    '''
    
    # set objects for storing metrics
    best_loss = 1e10
    train_losses = []
    valid_losses = []
 
    # Train model
    for epoch in range(0, epochs):

        # training
        model, optimizer, train_loss = train(train_loader, model, criterion, optimizer, device)
        train_losses.append(train_loss)

        # validation
        with torch.no_grad():
            model, valid_loss = validate(valid_loader, model, criterion, device)
            valid_losses.append(valid_loss)

        if epoch % print_every == (print_every - 1):
            
            train_acc = get_accuracy(model, train_loader, device=device)
            valid_acc = get_accuracy(model, valid_loader, device=device)
            
            print(f'E: {epoch:02d} P: {prune.getPruneAmount(model).item():.3f}\t'
                  f'Train loss: {train_loss:.4f}\t'
                  f'Valid loss: {valid_loss:.4f}\t'
                  f'Train acc: {100 * train_acc:.2f}\t'
                  f'Valid acc: {100 * valid_acc:.2f}')
    
    return model, optimizer, (train_losses, valid_losses)


def training_complete(model, criterion, optimizer, train_loader, valid_loader, device, train_for=1, delta=0.1, print_every=1):
    '''
    Trains model 1 {train_for} epoch repeatedly until < 0.1 {delta} improvement
    '''
    
    # set objects for storing metrics
    best_loss = 1e10
    train_losses = []
    valid_losses = []
    epoch = 0

    prev_loss = 0
 
    # Train model
    while True:

        # training
        for i in range(train_for):
            model, optimizer, train_loss = train(train_loader, model, criterion, optimizer, device)
            train_losses.append(train_loss)

        # validation
        with torch.no_grad():
            model, valid_loss = validate(valid_loader, model, criterion, device)
            valid_losses.append(valid_loss)

        train_acc = get_accuracy(model, train_loader, device=device)
        valid_acc = get_accuracy(model, valid_loader, device=device)

        if epoch % print_every == (print_every - 1):
            
            print(f'E: {epoch:02d} P: {prune.getPruneAmount(model).item():.3f}\t'
                  f'Train loss: {train_loss:.4f}\t'
                  f'Valid loss: {valid_loss:.4f}\t'
                  f'Train acc: {100 * train_acc:.2f}\t'
                  f'Valid acc: {100 * valid_acc:.2f}', end='\r')
        epoch += 1

        if valid_acc * 100 - prev_loss < delta:
            break
        prev_loss = valid_acc * 100
    print(f'\nDense Training took: {epoch * len(train_loader)} iterations')
    
    return model, optimizer, (train_acc, valid_acc), epoch * len(train_loader)

def training_complete_iter(model, criterion, optimizer, train_loader, valid_loader, device, train_for=100, print_every=1):
    '''
    Trains model 100 {train_for} iterations repeatedly until test loss > 1.2 min test loss or iter > 15000
    '''
    
    # set objects for storing metrics
    best_loss = 10000000
    train_losses = []
    valid_losses = []
    round = 0

    iterCount = 0
    bestIter = 0
    bestAcc = 0

    bestModel = None;
 
    # Train model
    while True:

        # training
        model, optimizer, train_loss = trainIter(train_loader, model, criterion, optimizer, device, amount=train_for)
        # train_losses.append(train_loss)

        # # validation
        with torch.no_grad():
            model, valid_loss = validate(valid_loader, model, criterion, device, count=50)
            valid_losses.append(valid_loss)

        # Get Quick accuracy check since it is not final
        valid_acc = get_accuracy(model, valid_loader, device=device, count=10)

        iterCount += train_for

        if round % print_every == (print_every - 1):
            print(f'I:{iterCount/1000:.1f}k P:{prune.getPruneAmount(model).item():.3f}\t'
                f'Train loss: {train_loss:.4f}\t'
                f'Valid loss: {valid_loss:.4f}\t'
                f'Valid acc: {100 * valid_acc:.2f}        ', end='\r')
        round += 1

        if valid_loss > best_loss * 1.2 or iterCount > 15000: # Limit # of iterations
            break
        elif valid_loss < best_loss:
            best_loss = valid_loss
            bestIter = iterCount
            bestModel = copy.deepcopy(model.state_dict())
    
    model.load_state_dict(bestModel)
    bestAcc = get_accuracy(model, valid_loader, device=device)

    print(f'I:{bestIter/1000:.1f}k P:{prune.getPruneAmount(model).item():.3f}\t'
        f'Train loss: {train_loss:.4f}\t'
        f'Valid loss: {best_loss:.4f}\t'
        f'Valid acc: {100 * bestAcc:.2f}', end='\r')
    print(f'\nTraining took: {iterCount} iterations\t'
        f'Min loss at: {bestIter}')
    
    return model, optimizer, (train_losses, valid_losses), bestIter, 100 * bestAcc


def train(train_loader, model, criterion, optimizer, device):
    '''
    Train for one epoch
    '''

    model.train()
    running_loss = 0
    
    for X, y_true in train_loader:

        optimizer.zero_grad()
        
        X = X.to(device)
        y_true = y_true.to(device)
    
        # Forward pass
        y_hat, _ = model(X) 
        loss = criterion(y_hat, y_true) 
        running_loss += loss.item() * X.size(0)

        # Backward pass
        loss.backward()
        optimizer.step()
        
    epoch_loss = running_loss / len(train_loader.dataset)
    return model, optimizer, epoch_loss

def trainIter(train_loader, model, criterion, optimizer, device, amount=100):
    '''
    Train for X iterations. Default: 100 iterations
    '''

    model.train()
    cur_loss = 0
    
    for i in range(amount):
        X, y_true = next(iter(train_loader))

        optimizer.zero_grad()
        
        X = X.to(device)
        y_true = y_true.to(device)
    
        # Forward pass
        y_hat, _ = model(X) 
        loss = criterion(y_hat, y_true) 
        cur_loss += loss.item() * X.size(0)

        # Backward pass
        loss.backward()
        optimizer.step()
        
    epoch_loss = cur_loss / len(train_loader.dataset)
    return model, optimizer, epoch_loss
